package com.example.mypokerplanning.multicast

import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

class MulticastPublisher {
    private var socket: DatagramSocket? = null
    private var group: InetAddress? = null
    private var buf: ByteArray? = null

    @Throws(IOException::class)
    fun multicast(multicastMessage: String) {
        socket = DatagramSocket()
        group = InetAddress.getByName("228.1.2.6")
        buf = multicastMessage.toByteArray()

        val packet = DatagramPacket(buf, buf!!.size, group, 4446)
        socket!!.send(packet)
        socket!!.close()
    }
}